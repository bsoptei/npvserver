use actix_web::{App, HttpServer};
use npvserver::*;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| App::new().configure(configure_endpoints))
        .bind(host_port().await.to_string())?
        .run()
        .await
}
