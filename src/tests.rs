#[cfg(test)]
mod tests {
    use crate::*;
    use actix_web::{test, web, App};
    use maplit::*;
    use DiscountRateByTenor::*;

    const TOLERANCE: f64 = 1e-5;

    fn compare(expected: f64, actual: f64) {
        assert!(
            (expected - actual).abs() <= TOLERANCE,
            "expected: {}, actual: {}",
            expected,
            actual
        );
    }

    #[actix_web::test]
    async fn test_pv() {
        compare(95.238095, pv(100.0, 0.05, 1));
    }

    #[actix_web::test]
    async fn test_npv() {
        let cash_flows = || vec![100.0, -50.0, 35.0];

        let input1 = CashFlowsAndDiscountRates::new(cash_flows(), SingleDiscountRate(0.05));
        compare(84.12698412, npv(input1).await);

        let input2 = CashFlowsAndDiscountRates::new(
            cash_flows(),
            ListedDiscountRate(vec![0.05, 0.06, 0.07]),
        );
        compare(83.40054416, npv(input2).await);
        let input3 =
            CashFlowsAndDiscountRates::new(cash_flows(), ListedDiscountRate(vec![0.05, 0.06]));
        compare(87.83018867, npv(input3).await);

        let input4 = CashFlowsAndDiscountRates::new(cash_flows(), MappedDiscountRate(hashmap![]));
        compare(85.0, npv(input4).await);
        let input5 = CashFlowsAndDiscountRates::new(
            cash_flows(),
            MappedDiscountRate(
                hashmap![String::from("0") => 0.05, String::from("1") => 0.06, String::from("2") => 0.07],
            ),
        );
        compare(83.40054416, npv(input5).await);
    }

    use serde::{de::DeserializeOwned, Serialize};

    fn roundtrip<S: DeserializeOwned + Serialize>(s: &S) -> S {
        serde_json::from_str(&serde_json::to_string(&s).unwrap()).unwrap()
    }

    #[actix_web::test]
    async fn serde_roundtrips() {
        let sdr = SingleDiscountRate(0.05);
        assert_eq!(sdr, roundtrip(&sdr));

        let ldr = ListedDiscountRate(vec![0.05]);
        assert_eq!(ldr, roundtrip(&ldr));
        let ldr2 = ListedDiscountRate(vec![]);
        assert_eq!(ldr2, roundtrip(&ldr2));

        let mdr = MappedDiscountRate(hashmap![String::from("0") => 0.05]);
        assert_eq!(mdr, roundtrip(&mdr));
        let mdr2 = MappedDiscountRate(hashmap![]);
        assert_eq!(mdr2, roundtrip(&mdr2));

        let cfdr1 = CashFlowsAndDiscountRates::new(vec![100.0], sdr);
        assert_eq!(cfdr1, roundtrip(&cfdr1));
    }

    #[actix_web::test]
    async fn app_endpoints() {
        let mut app = test::init_service(App::new().configure(configure_endpoints)).await;

        let data =
            CashFlowsAndDiscountRates::new(vec![10.0, 20.0], ListedDiscountRate(vec![0.05, 0.06]));
        let post_req = test::TestRequest::post()
            .uri(NPV_ENDPOINT_PATTERN)
            .set_json(&data)
            .to_request();
        let resp_from_post = test::call_service(&mut app, post_req).await;
        assert!(resp_from_post.status().is_success());

        let body_from_post = test::read_body(resp_from_post).await;
        assert_eq!(
            web::Bytes::from_static("28.867924528301884".as_bytes()),
            body_from_post
        );
    }

    #[actix_web::test]
    async fn test_host_port() {
        use std::env;

        let custom_host = "1.1.1.1";
        let custom_port = "5000";
        env::set_var(HOST_VAR_NAME, custom_host);
        env::set_var(PORT_VAR_NAME, custom_port);
        let result = host_port().await;
        assert_eq!(
            format!("{}:{}", custom_host, custom_port),
            result.to_string()
        );
        env::remove_var(HOST_VAR_NAME);
        env::remove_var(PORT_VAR_NAME);
        let result2 = host_port().await;
        assert_eq!(
            format!("{}:{}", DEFAULT_HOST, DEFAULT_PORT),
            result2.to_string()
        );
    }
}
