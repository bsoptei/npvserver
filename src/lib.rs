use actix_web::{
    web::{post, Json, ServiceConfig},
    HttpResponse,
};
use serde::{Deserialize, Serialize};
use std::{collections::HashMap, env};

mod tests;

pub async fn npv_endpoint(cfdr: Json<CashFlowsAndDiscountRates>) -> HttpResponse {
    match serde_json::to_string(&npv(cfdr.into_inner()).await) {
        Ok(value) => HttpResponse::Ok().body(value),
        Err(err) => HttpResponse::Ok().body(format!("{:?}", err)),
    }
}

pub const NPV_ENDPOINT_PATTERN: &str = "/npv";
pub const DEFAULT_HOST: &str = "0.0.0.0";
pub const DEFAULT_PORT: &str = "8080";
pub const HOST_VAR_NAME: &str = "HOST";
pub const PORT_VAR_NAME: &str = "PORT";

pub fn configure_endpoints(cfg: &mut ServiceConfig) {
    cfg.route(NPV_ENDPOINT_PATTERN, post().to(npv_endpoint));
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct CashFlowsAndDiscountRates {
    cash_flows: Vec<f64>,
    discount_rates: DiscountRateByTenor,
}

impl CashFlowsAndDiscountRates {
    pub fn new(cash_flows: Vec<f64>, discount_rates: DiscountRateByTenor) -> Self {
        Self {
            cash_flows,
            discount_rates,
        }
    }
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[serde(untagged)]
pub enum DiscountRateByTenor {
    SingleDiscountRate(f64),
    ListedDiscountRate(Vec<f64>),
    MappedDiscountRate(HashMap<String, f64>),
}

impl DiscountRateByTenor {
    pub fn discount_rate(&self, tenor: usize) -> f64 {
        use DiscountRateByTenor::*;

        match self {
            SingleDiscountRate(value) => *value,
            ListedDiscountRate(list) => list.get(tenor).copied().unwrap_or_default(),
            MappedDiscountRate(mapped) => {
                mapped.get(&tenor.to_string()).copied().unwrap_or_default()
            }
        }
    }
}

pub struct HostPort {
    host: String,
    port: String,
}

impl HostPort {
    pub fn new(host: String, port: String) -> Self {
        Self { host, port }
    }
}

impl ToString for HostPort {
    fn to_string(&self) -> String {
        format!("{}:{}", self.host, self.port)
    }
}

pub fn pv(amount: f64, discount_rate: f64, period: u64) -> f64 {
    amount / (1f64 + discount_rate).powf(period as f64)
}

pub async fn npv(cfdr: CashFlowsAndDiscountRates) -> f64 {
    cfdr.cash_flows
        .iter()
        .enumerate()
        .map(|(period, amount)| {
            pv(
                *amount,
                cfdr.discount_rates.discount_rate(period),
                period as u64,
            )
        })
        .sum()
}

pub async fn host_port() -> HostPort {
    HostPort::new(
        env::var(HOST_VAR_NAME)
            .ok()
            .unwrap_or_else(|| DEFAULT_HOST.to_owned()),
        env::var(PORT_VAR_NAME)
            .ok()
            .unwrap_or_else(|| DEFAULT_PORT.to_owned()),
    )
}
